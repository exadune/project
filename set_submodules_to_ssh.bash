#!/bin/bash

git submodule foreach 'git remote set-url origin $(git config --get remote.origin.url | sed "s;https://gitlab.dune-project.org;ssh://git@gitlab.dune-project.org:22022;g")'
